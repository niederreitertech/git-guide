# Git Init: Einführung in unsere Arbeitsweise mit Git

**Anfänger: Bitte lest bei Bedarf zuerst die "Anfänger"-Abschnitte (weiter unten). Aber auch die "Fortgeschrittene"-Abschnitte sind für euch verbindlich.**


## Fortgeschrittene: Format der Commit-Nachricht

Für Cleansumo gilt ein vorgegebenes Format, damit die Nachrichten ein Mindestniveau an Aussagekraft und Einheitlichkeit bieten.

Die Konventionen sind u.a. hier festgelegt: http://karma-runner.github.io/0.10/dev/git-commit-msg.html

```
<type>(<scope>): <subject>

<body>

<footer>
```

Beispielnachricht: `chore(swagger-ui): swagger-ui initial upload`

## Fortgeschrittene: Branching

Um über mehrere Stunden hinweg oder länger an einer Funktionalität zu arbeiten, ist die Nutzung von **Branches** empfehlenswert.

Ich möchte auf dem Komplexitätsniveau der Variante "GitFlow Light" (siehe http://www.syntevo.com/smartgit/whatsnew) mit euch arbeiten (ohne GitFlow dafür jedoch nutzen zu müssen): Es gibt die Master-Branch, *keine* Develop-Branch und die Branches der einzelnen Entwickler, meist wird es sich dabei um Feature-Branches handeln.
Wir nutzen von Anfang an [Rebasing](https://www.atlassian.com/git/articles/git-team-workflows-merge-or-rebase), um eine übersichtliche Chronik zu erreichen.

Wir könnten dafür GitFlow nutzen, wobei aber wenig dafür steht:
Denn GitFlow führt viel Komplexität ein, die wir gar nicht brauchen. Wer aber will, kann dennoch sehr gerne GitFlow nutzen und Feature-Branches bei Abschluss **rebasen** (dann erst mergen).

Vorerst möchte ich aber nur die pure Vorgehensweise ohne Zusatztools vorstellen, um keine Verwirrung zu stiften:

1. `git branch feature/myfeat`
2. `git checkout feature/myfeat`
3. Am Feature arbeiten und einen oder mehrere Commits durchführen.
4. `git rebase master` = ausgehend vom mittlerweile neusten Master-Commit werden alle Feature-Commits neu durchgeführt
5. `git checkout master`
6. `git merge feature/myfeat` = die erneut durchgeführten Feature-Commits werden an die Master-Branch angehängt

Der sechste Schritt ist kein Merge, der eine neue Linie in der Chronik einführt. Eine neue Linie ist nicht notwendig, da die Commits alle an das Ende angehängt werden. Dieser Merge sieht daher nicht wie ein üblicher Merge ohne Rebasing aus.

### Golden Rule of Rebasing

Bitte beachtet, dass Rebasing grundsätzlich gefährlich ist: Ihr könnt damit ordentlich Verwirrung stiften. Führt Rebase-Vorgänge daher anfangs bitte nach Möglichkeit überlegt aus (also weniger Try&Error, mehr bedächtig), solltet ihr damit noch unerfahren sein. *Es sollte nicht passieren, dass die Master-Commits ein Rebase erfahren: Denn dann wissen eure Kollegen nicht, wie ihnen geschieht.*

Bitte lest hier rein, wenn ihr mehr zum Thema erfahren wollt: https://www.atlassian.com/git/tutorials/merging-vs-rebasing/the-golden-rule-of-rebasing


## Anfänger: Mit einem Projekt beginnen

### Neues Projekt, oder bestehendes Projekt ohne Git

Im Projektverzeichnis: `git init`

#### Repo anlegen

Entweder via Browser auf GitHub/Bitbucket/... oder folgendermaßen auf Festplatte/USB-Stick/eigenem Server:

```
$ mkdir myproject.git
$ cd myproject.git
$ git init --bare
```

#### Projekt mit dem Repo verbinden

##### Festplatte/USB-Stick/...:

```
$ cd myproject
$ git remote -v # should return nothing
$ git remote add origin /path/to/myproject.git
$ git remote -v # should now return:
origin	/path/to/myproject.git (fetch)
origin	/path/to/myproject.git (push)
```

##### GitHub/Bitbucket/...:

Für die Remote-Authentifizierung wird ein SSH-Schlüssel benötigt.

Daher Schlüsselpaar generieren: `ssh-keygen -t rsa` (für Windows gibt es z.B. hier eine Anleitung: http://guides.beanstalkapp.com/version-control/git-on-windows.html)

Die resultierende **id_rsa.pub**-Datei enthält den öffentlichen Schlüssel in Textform. Diesen Schlüssel trägt man auf Bitbucket hier ein: https://bitbucket.org/account/user/yourusername/ssh-keys


```
$ cd myproject
$ git remote -v # should return nothing
$ git remote add origin git@bitbucket.org:bitbucketuser/myproject.git
$ git remote -v # should now return:
origin	git@bitbucket.org:bitbucketuser/myproject.git (fetch)
origin	git@bitbucket.org:bitbucketuser/myproject.git (push)
```

### Bestehendes Projekt: Pull aus Git-Repo

Ebenso muss auch hierfür ein Schlüsselpaar generiert werden (siehe oben). Im Anschluss daran kann das Repo geklont werden:

```
$ git clone git@bitbucket.org:bitbucketuser/myproject.git
Cloning into 'myproject'...
remote: Counting objects: 592, done.
remote: Compressing objects: 100% (509/509), done.
remote: Total 592 (delta 243), reused 0 (delta 0)
Receiving objects: 100% (592/592), 271.51 KiB | 0 bytes/s, done.
Resolving deltas: 100% (243/243), done.
Checking connectivity... done.
```

## Anfänger: Grundlegender Arbeitsablauf

Einmaliges Namen-Einstellen:

* `git config --global user.name My Names`
* `git config --global user.email my@email.address`

Arbeiten im Projektverzeichnis:

1. `git pull origin master` = Neues ziehen
2. Änderungen in den Dateien vornehmen
3. `git status` (optional) ansehen, welche Dateien eingetragen werden könnten
4. `git add directory-or-file` = stage (Bereitstellen)
5. `git commit -m "my commit message"` = Bereitgestelltes eintragen
6. `git pull origin master` = inzwischen entstandene Änderungen der anderen ziehen
7. `git push origin master` = Versenden

### GUI-Werkzeuge

Äquivalente zu den oben genannten Kommandozeilenbefehlen:

* `git pull origin master` = Knopf **Pull**
* `git add` = Knopf/Menüeintrag **Stage**
* `git commit` = Knopf **Commit**
* `git push origin master` = Knopf **Push**

Weitere Äquivalente für die Bedienung von Feature-Branches (Gebrauch: siehe "Fortgeschrittene: Branching"):

* `git branch` = **Branch**
* `git checkout` = **Checkout**
* `git rebase` = **Rebase**
* `git merge` = **Merge**

## Literatur

Sehr gute Tutorials und Artikel von Atlassian:

* https://www.atlassian.com/git/tutorials
* https://www.atlassian.com/git/articles/git-team-workflows-merge-or-rebase
* https://www.atlassian.com/git/tutorials/merging-vs-rebasing
* https://www.atlassian.com/git/tutorials/merging-vs-rebasing/the-golden-rule-of-rebasing

Schöner Slideshare:

* http://www.slideshare.net/TrungHuynh5/git-for-beginner-46199029

Möglicherweise auch empfehlenswert:

* http://backlogtool.com/git-guide/en

Schöne Cheatsheets:

* https://de.atlassian.com/dms/wac/images/landing/git/atlassian_git_cheatsheet.pdf
* https://www.git-tower.com/blog/git-cheat-sheet

GitFlow: Nicht das Gelbe vom Ei?

* http://endoflineblog.com/gitflow-considered-harmful
* http://endoflineblog.com/follow-up-to-gitflow-considered-harmful
